import React from "react";
import { Route, Routes, Navigate } from "react-router-dom";
import { withStyles } from "@mui/styles";
import Container from "@material-ui/core/Container";
import styles from "./styles";

import DetailToko from "./Component/detail-toko/index";
import DetailProduct from "./Component/detail-product/index";
import DetailCategory from "./Component/list-product/index";

function App(props) {
  const { classes } = props;
  return (
    <Container maxWidth={false} className={classes.containerApp}>
      <Routes>
        <Route
          path="/category/:category/:id"
          exact
          element={<DetailProduct />}
        />
        <Route path="/category/:category" exact element={<DetailCategory />} />
        <Route path="/category" exact element={<DetailToko />} />
        <Route path="/" element={<Navigate replace to="/category" />} />
      </Routes>
    </Container>
  );
}

export default withStyles(styles)(App);
