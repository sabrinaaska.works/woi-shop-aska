const styles = (theme) => ({
  //App
  containerApp: {
    position: "relative !important",
    padding: "4rem 0 0 0 !important",
    display: "flex !important",
    flexDirection: "column !important",
    alignItems: "center !important",
  },

  //Header
  containerHeader: {
    top: 0,
    position: "fixed !important",
    maxWidth: "500px !important",
    padding: "10px  16px !important",
    zIndex: "100 !important",

    display: "flex !important",
    alignItems: "flex-start !important",
    justifyContent: "space-between !important",

    gap: "10px",
    boxShadow: "0px 2px 8px rgba(0, 0, 0, 0.04)",
    "@media screen and (max-width: 560px)": {
      fontSize: "14px !important",
    },
    backgroundColor: theme.palette.custom.White,

    "@media screen and (max-width: 285px)": {
      maxWidth: "100% !important",
      width: "100% !important",
    },
  },

  boxHeader: {
    display: "flex !important",
    alignItems: "center !important",
    justifyContent: "center !important",
  },

  arrowBack: {
    color: "#03AC0E !important",
  },

  textNav: {
    marginLeft: "10px !important",
    fontWeight: "600 !important",
    lineHeight: "24px !important",
    color: theme.palette.custom.BlackText,

    "@media screen and (max-width: 560px)": {
      fontSize: "14px !important",
      lineHeight: "22px !important",
    },
  },

  searchIcon: {
    color: theme.palette.custom.BlackText,
  },

  //List
  containerProduct: {
    margin: "20px 0 !important",
    display: "grid !important",
    gridGap: "10px !important",
    gridTemplateColumns: "repeat(auto-fill, minmax(125px, 1fr)) !important",
  },

  listBox: {
    cursor: "pointer !important",
  },

  thumbnailProduct: {
    width: "100% !important",
    height: "125px !important",
    objectFit: "cover !important",
    border: "1px solid #EBEBEB !important",
    borderRadius: "10px",
  },

  titleProduct: {
    display: "inline-block !important",
    width: "100% !important",
    margin: "10px 0 !important",
    fontWeight: "400 !important",
    overflow: "hidden !important",
    whiteSpace: "nowrap",
    textOverflow: "ellipsis !important",
    color: theme.palette.custom.BlackText,

    "@media screen and (max-width: 560px)": {
      fontSize: "14px !important",
      lineHeight: "22px !important",
    },
  },

  priceProduct: {
    fontWeight: "600 !important",
    fontSize: "18px !important",
    lineHeight: "21px !important",
    color: theme.palette.custom.BlackText,

    "@media screen and (max-width: 560px)": {
      fontSize: "16px !important",
      lineHeight: "19px !important",
    },
  },

  buttonAdd: {
    margin: "20px 0 !important",
    width: "100% !important",
    height: "40px !important",

    color: `${theme.palette.custom.FirstGreen} !important`,
    border: `1px solid ${theme.palette.custom.FirstGreen} !important`,
    borderRadius: "100px !important",
    textTransform: "capitalize !important",
    fontWeight: "600 !important",

    "@media screen and (max-width: 560px)": {
      fontSize: "14px !important",
    },
  },
});

export default styles;
