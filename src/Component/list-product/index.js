import withStyles from "@mui/styles/withStyles";
import Component from "./component";
import styles from "./styles";

const Styled = withStyles(styles)(Component);
export default Styled;
