import React from "react";
import { useNavigate } from "react-router-dom";
import { Container, Typography, Button } from "@mui/material";
import NumberFormat from "react-number-format";

function ListProductMakanan(props) {
  const { style, data } = props;
  const navigate = useNavigate();

  return (
    <Container className={style.containerProduct}>
      {data?.map((data, index) => (
        <div key={index}>
          <div
            className={style.listBox}
            key={data.id}
            onClick={() => {
              navigate(`${data.id}`);
            }}
          >
            <img
              className={style.thumbnailProduct}
              src={data.thumbnail}
              alt="thumbnail"
            />
            <Typography className={style.titleProduct}>{data.title}</Typography>
            <Typography className={style.priceProduct}>
              <NumberFormat
                value={data.price}
                displayType={"text"}
                prefix={"$"}
              />
            </Typography>
          </div>
          <Button variant="outlined" className={style.buttonAdd}>
            Tambah
          </Button>
        </div>
      ))}
    </Container>
  );
}

export default ListProductMakanan;
