import React, { useState, useEffect } from "react";
import { useParams } from "react-router-dom";
import axios from "axios";
import Header from "./header/index";
import List from "./list-product/index";
import EmptyPage from "../list-product-empty/index";
import Filter from "../filter/index";

import { Container } from "@mui/material";

function Component(props) {
  const { classes } = props;

  //get details product
  const { category } = useParams();
  const [products, setProducts] = useState([]);

  useEffect(() => {
    const fetch = async () => {
      try {
        const { data } = await axios.get(
          `https://dummyjson.com/products/category/${category}`
        );
        setProducts(data.products);
      } catch (error) {
        console.log(error);
      }
    };
    fetch();
  }, [category]);

  let length = products?.length;

  return (
    <div>
      {length < 0 ? (
        <Container className={classes.containerApp}>
          <Header category={category} style={classes} />
          <EmptyPage />
        </Container>
      ) : (
        <Container className={classes.containerApp}>
          <Header category={category} style={classes} />
          <Filter />
          <List data={products} style={classes} />
        </Container>
      )}
    </div>
  );
}

export default Component;
