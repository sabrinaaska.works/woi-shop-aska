import React from "react";
import { useNavigate } from "react-router-dom";

import { Container, Box, Typography, IconButton } from "@mui/material";
import SearchIcon from "@mui/icons-material/Search";
import ArrowBackIcon from "@mui/icons-material/ArrowBack";

function HeaderListProdukMakanan(props) {
  const { style, category } = props;
  const navigate = useNavigate();
  return (
    <Container className={style.containerHeader}>
      <Box className={style.boxHeader}>
        <IconButton onClick={() => navigate(-1)}>
          <ArrowBackIcon className={style.arrowBack} />
        </IconButton>
        <Typography className={style.textNav}>{category}</Typography>
      </Box>
      <Box className={style.boxApp}>
        <IconButton>
          <SearchIcon className={style.searchIcon} />
        </IconButton>
      </Box>
    </Container>
  );
}

export default HeaderListProdukMakanan;
