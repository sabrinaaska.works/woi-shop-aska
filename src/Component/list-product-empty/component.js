import React from "react";
import { Container, Typography } from "@mui/material";
import image from "./empty.jpg";

function Component(props) {
  const { classes } = props;
  return (
    <Container className={classes.containerApp}>
      <img src={image} alt="empty" className={classes.imgEmpty} />

      <Typography className={classes.textTitle}>
        Produk Tidak Ditemukan
      </Typography>
      <Typography className={classes.textEmpty}>
        Maaf, sepertinya produk yang Anda cari tidak ditemukan, coba cari yang
        lain yuk!
      </Typography>
    </Container>
  );
}

export default Component;
