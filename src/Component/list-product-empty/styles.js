const styles = (theme) => ({
  //App
  containerApp: {
    height: "100% !important",
    margin: "0 !important",
    padding: "0 16px !important",
    display: "flex !important",
    flexDirection: "column !important",
    alignItems: "center !important",
  },

  imgEmpty: {
    marginTop: "55px !important",
    width: "260px !important",
  },

  textTitle: {
    marginTop: "20px !important",
    fontWeight: "600 !important",
    lineHeight: "24px !important",
    color: theme.palette.custom.BlackText,
    "@media screen and (max-width: 560px)": {
      fontSize: "14px !important",
      lineHeight: "22px !important",
    },
  },

  textEmpty: {
    marginTop: "20px !important",
    width: "260px !important",
    textAlign: "center !important",
    fontWeight: "400 !important",
    lineHeight: "24px !important",
    color: theme.palette.custom.Grey,
    "@media screen and (max-width: 560px)": {
      fontSize: "14px !important",
      lineHeight: "22px !important",
    },
  },
});

export default styles;
