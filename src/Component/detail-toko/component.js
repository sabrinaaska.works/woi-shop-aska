import React from "react";
import Header from "./header/index";
import List from "./list-card/index";
import Category from "./category/index";
import Button from "./button/index";

import { Container } from "@mui/material";

function Component(props) {
  const { classes } = props;
  return (
    <Container className={classes.containerApp}>
      <Header style={classes} />
      <List style={classes} />
      <div className={classes.rectangle}></div>
      <Category style={classes} />
      <Button style={classes} />
    </Container>
  );
}

export default Component;
