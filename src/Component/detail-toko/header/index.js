import React from "react";

import { Container, Box, Typography, IconButton } from "@mui/material";
import SearchIcon from "@mui/icons-material/Search";
import ArrowBackIcon from "@mui/icons-material/ArrowBack";

function HeaderDetailToko(props) {
  const { style } = props;
  return (
    <Container className={style.containerHeader}>
      <Box className={style.boxHeader}>
        <IconButton>
          <ArrowBackIcon className={style.arrowBack} />
        </IconButton>
        <Typography className={style.textNav}>Detail Toko</Typography>
      </Box>
      <Box className={style.boxApp}>
        <IconButton>
          <SearchIcon className={style.searchIcon} />
        </IconButton>
      </Box>
    </Container>
  );
}

export default HeaderDetailToko;
