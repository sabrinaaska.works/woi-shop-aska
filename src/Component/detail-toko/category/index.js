import React, { useContext } from "react";
import { useNavigate } from "react-router-dom";
import { Container, Box, Typography } from "@mui/material";
import { TokoContext } from "../../../Context/ListProductToko";

function CategoryDetailToko(props) {
  const { style } = props;
  const { category } = useContext(TokoContext);
  const navigate = useNavigate();

  return (
    <Container className={style.containerCategory}>
      <Typography className={style.titleCategory}>Kategori Produk</Typography>

      <Box className={style.boxCategory}>
        {category.map((data, index) => (
          <div
            key={index}
            className={style.link}
            onClick={() => {
              navigate(`/category/${data}`);
            }}
          >
            <div className={style.wrapperCategory}>
              {data === "smartphones" ? (
                <img
                  className={style.imgCategory}
                  src="./img-category/smartphones.png"
                  alt="category"
                />
              ) : data === "laptops" ? (
                <img
                  className={style.imgCategory}
                  src="./img-category/laptops.png"
                  alt="category"
                />
              ) : data === "fragrances" ? (
                <img
                  className={style.imgCategory}
                  src="./img-category/fragrances.png"
                  alt="category"
                />
              ) : data === "skincare" ? (
                <img
                  className={style.imgCategory}
                  src="./img-category/skincare.png"
                  alt="category"
                />
              ) : data === "groceries" ? (
                <img
                  className={style.imgCategory}
                  src="./img-category/groceries.png"
                  alt="category"
                />
              ) : data === "home-decoration" ? (
                <img
                  className={style.imgCategory}
                  src="./img-category/home-decoration.png"
                  alt="category"
                />
              ) : data === "furniture" ? (
                <img
                  className={style.imgCategory}
                  src="./img-category/furniture.png"
                  alt="category"
                />
              ) : data === "tops" ? (
                <img
                  className={style.imgCategory}
                  src="./img-category/tops.png"
                  alt="category"
                />
              ) : data === "womens-dresses" ? (
                <img
                  className={style.imgCategory}
                  src="./img-category/women-dresses.png"
                  alt="category"
                />
              ) : data === "womens-shoes" ? (
                <img
                  className={style.imgCategory}
                  src="./img-category/women-shoes.png"
                  alt="category"
                />
              ) : data === "mens-shirts" ? (
                <img
                  className={style.imgCategory}
                  src="./img-category/mens-shirts.png"
                  alt="category"
                />
              ) : data === "mens-shoes" ? (
                <img
                  className={style.imgCategory}
                  src="./img-category/mens-shoes.png"
                  alt="category"
                />
              ) : data === "mens-watches" ? (
                <img
                  className={style.imgCategory}
                  src="./img-category/mens-watches.png"
                  alt="category"
                />
              ) : data === "womens-watches" ? (
                <img
                  className={style.imgCategory}
                  src="./img-category/womens-watches.png"
                  alt="category"
                />
              ) : data === "womens-bags" ? (
                <img
                  className={style.imgCategory}
                  src="./img-category/womens-bags.png"
                  alt="category"
                />
              ) : data === "womens-jewellery" ? (
                <img
                  className={style.imgCategory}
                  src="./img-category/womens-jewellery.png"
                  alt="category"
                />
              ) : data === "sunglasses" ? (
                <img
                  className={style.imgCategory}
                  src="./img-category/sunglasses.png"
                  alt="category"
                />
              ) : data === "automotive" ? (
                <img
                  className={style.imgCategory}
                  src="./img-category/automotive.png"
                  alt="category"
                />
              ) : data === "motorcycle" ? (
                <img
                  className={style.imgCategory}
                  src="./img-category/motorcycle.png"
                  alt="category"
                />
              ) : data === "lighting" ? (
                <img
                  className={style.imgCategory}
                  src="./img-category/lighting.png"
                  alt="category"
                />
              ) : (
                <img
                  className={style.imgCategory}
                  src="./list-category/photo_2022-08-16_22-17-53.jpg"
                  alt="category"
                />
              )}
              <span className={style.textCategory}>{data}</span>
            </div>
          </div>
        ))}
      </Box>
    </Container>
  );
}

export default CategoryDetailToko;
