import React from "react";

import { Container, Box, Typography, Grid } from "@mui/material";
import FavoriteIcon from "@mui/icons-material/Favorite";

function ListCardDetailToko(props) {
  const { style } = props;
  return (
    <Container className={style.containerList}>
      <Grid container spacing={1} className={style.gridContainer}>
        <Grid item xs={3} className={style.left}>
          <Box className={style.logoBox}>
            <img
              className={style.imgLogo}
              src="./logo/logo-toko_semesta.jpg"
              alt="logo-toko"
            />
            <div className={style.wrapperScore}>
              <FavoriteIcon className={style.favIcon} />
              <Typography className={style.textScore}>5.0</Typography>
            </div>
          </Box>
        </Grid>
        <Grid item xs={9} className={style.right}>
          <Box className={style.detailContentBox}>
            <Typography className={style.titleContent}>Toko Semesta</Typography>
            <Typography className={style.textContent}>
              Jl. Kasipah Raya No. 182, Jatingaleh, Kec. Candisari, Kota
              Semarang
            </Typography>
            <Box className={style.buttonBox}>
              <button className={`${style.buttonContent} ${style.typeButton}`}>
                Express
              </button>
              <button className={`${style.buttonContent} ${style.timeButton}`}>
                09.00
              </button>
              <button className={`${style.buttonContent} ${style.timeButton}`}>
                16.00
              </button>
            </Box>
          </Box>
        </Grid>
      </Grid>
    </Container>
  );
}

export default ListCardDetailToko;
