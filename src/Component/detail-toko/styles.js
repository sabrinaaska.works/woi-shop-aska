const styles = (theme) => ({
  rectangle: {
    width: "100% !important",
    height: "10px !important",
    backgroundColor: theme.palette.custom.Rectangle,
  },

  link: {
    outline: "none !important",
    textDecoration: "none  !important",
  },

  //App
  containerApp: {
    position: "relative !important",
    padding: "4rem 0 0 0 !important",
    display: "flex !important",
    flexDirection: "column !important",
    alignItems: "center !important",
  },

  //Header
  containerHeader: {
    top: 0,
    position: "fixed !important",
    maxWidth: "500px !important",
    padding: "10px  16px !important",
    zIndex: "100 !important",

    display: "flex !important",
    alignItems: "flex-start !important",
    justifyContent: "space-between !important",

    gap: "10px",
    boxShadow: "0px 2px 8px rgba(0, 0, 0, 0.04)",
    "@media screen and (max-width: 560px)": {
      fontSize: "14px !important",
    },
    backgroundColor: theme.palette.custom.White,

    "@media screen and (max-width: 285px)": {
      maxWidth: "100% !important",
      width: "100% !important",
    },
  },

  boxHeader: {
    display: "flex !important",
    alignItems: "center !important",
    justifyContent: "center !important",
  },

  arrowBack: {
    color: "#03AC0E !important",
  },

  textNav: {
    marginLeft: "10px !important",
    fontWeight: "600 !important",
    lineHeight: "24px !important",
    color: theme.palette.custom.BlackText,
  },

  searchIcon: {
    color: theme.palette.custom.BlackText,
  },

  //List
  containerList: {
    padding: "20px  16px !important",
    display: "flex !important",
    flexDirection: "row !important",
    "@media screen and (max-width: 285px)": {
      flexDirection: "column !important",
    },
  },

  logoBox: {
    position: "relative !important",
    display: "flex",
    justifyContent: "center",
    height: "fit-content !important",
  },

  imgLogo: {
    width: "111px !important",
    marginTop: "-20px !important",

    "@media screen and (max-width: 560px)": {
      width: "90px !important",
    },

    "@media screen and (max-width: 380px)": {
      width: "80px !important",
    },
  },

  wrapperScore: {
    position: "absolute !important",
    bottom: "-10% !important",
    width: "fit-content !important",
    padding: "6px 8px !important",
    borderRadius: "100px !important",

    display: "flex !important",
    flexDirection: "row !important",
    alignItems: "center !important",
    justifyContent: "center !important",
    backgroundColor: theme.palette.custom.FirstGreen,
  },

  gridContainer: {
    justifyContent: "center !important",
    alignItems: "center !important",
    marginLeft: "0 !important",
    "@media screen and (max-width: 285px)": {
      flexDirection: "column !important",
      alignItems: "flex-start !important",
    },
  },

  left: {
    "@media screen and (max-width: 560px)": {
      flexBasis: "30% !important",
      maxWidth: "30% !important",
    },

    "@media screen and (max-width: 380px)": {
      flexBasis: "25% !important",
      maxWidth: "25% !important",
    },

    "@media screen and (max-width: 285px)": {
      flexBasis: "100% !important",
      maxWidth: "100% !important",
      width: "100% !important",
    },
  },

  right: {
    "@media screen and (max-width: 560px)": {
      flexBasis: "70% !important",
      maxWidth: "70% !important",
    },

    "@media screen and (max-width: 380px)": {
      flexBasis: "75% !important",
      maxWidth: "75% !important",
    },

    "@media screen and (max-width: 285px)": {
      marginTop: "20px !important",
      flexBasis: "100% !important",
      maxWidth: "100% !important",
    },
  },

  favIcon: {
    fontSize: "22px !important",
    color: theme.palette.custom.White,

    "@media screen and (max-width: 560px)": {
      fontSize: "20px !important",
    },
  },

  textScore: {
    order: 1,
    flexGrow: 0,
    paddingLeft: "5px !important",

    fontSize: "16px !important",
    fontWeight: "600 !important",
    lineHeight: "18px !important",
    textAlign: "center !important",
    color: theme.palette.custom.BlackText,

    "@media screen and (max-width: 560px)": {
      fontSize: "14px !important",
      lineHeight: "16px !important",
    },
  },

  detailContentBox: {
    height: "100% !important",
    paddingLeft: "1rem !important",
    display: "flex !important",
    flexDirection: "column !important",
    justifyContent: "center !important",

    "@media screen and (max-width: 285px)": {
      paddingLeft: "0 !important",
    },
  },

  titleContent: {
    flexGrow: "0 !important",
    fontSize: "18px !important",
    fontWeight: "600 !important",
    lineHeight: "21px !important",
    color: theme.palette.custom.BlackText,
    "@media screen and (max-width: 560px)": {
      fontSize: "16px !important",
      lineHeight: "19px !important",
    },
  },

  textContent: {
    padding: "5px 0 !important",
    flexGrow: "0 !important",

    fontWeight: "400px !important",
    lineHeight: "18px !important",
    color: theme.palette.custom.Grey,

    "@media screen and (max-width: 560px)": {
      fontSize: "14px !important",
      lineHeight: "16px !important",
    },
  },

  buttonBox: {
    margin: "5px 0 !important",
    gap: "10px !important",
    display: "flex !important",
    flexDirection: "row !important",
    flexWrap: "wrap !important",
  },

  buttonContent: {
    display: "flex !important",
    alignItems: "center !important",
    padding: "8px 14px !important",
    gap: "8px !important",
    borderRadius: "100px !important",
    border: "none !important",

    fontSize: "14px !important",
    fontWeight: "600 !important",
    lineHeight: "14px !important",

    "@media screen and (max-width: 560px)": {
      padding: "8px 12px !important",
      gap: "5px !important",
      fontSize: "12px !important",
      lineHeight: "12px !important",
    },
  },

  typeButton: {
    color: theme.palette.custom.White,
    backgroundColor: theme.palette.custom.SecondGreen,
  },

  timeButton: {
    color: theme.palette.custom.FirstGreen,
    backgroundColor: theme.palette.custom.FifthGreen,
  },

  //Category
  containerCategory: {
    padding: "20px 16px !important",
  },

  titleCategory: {
    fontWeight: "600 !important",
    fontSize: "20px !important",
    lineHeight: "24px !important",
    marginBottom: "20px !important",
    color: theme.palette.custom.BlackText,
    "@media screen and (max-width: 560px)": {
      fontSize: "18px !important",
      lineHeight: "22px !important",
    },
  },

  boxCategory: {
    display: "grid !important",
    gridGap: "10px !important",
    gridTemplateColumns: "repeat(auto-fill, minmax(90px, 1fr)) !important",
    justifyItems: "center !important",
  },

  wrapperCategory: {
    width: "84px !important",
    display: "flex !important",
    alignItems: "center !important",
    flexDirection: "column !important",
    gap: "1rem !important",
    cursor: "pointer",
  },

  imgCategory: {
    width: "70% !important",
    "@media screen and (max-width: 560px)": {
      width: "60% !important",
    },
  },

  textCategory: {
    fontWeight: "400 !important",
    fontSize: "16px !important",
    textAlign: "center !important",
    lineHeight: "18px !important",
    color: theme.palette.custom.Grey,
    "@media screen and (max-width: 560px)": {
      fontSize: "14px !important",
      lineHeight: "16px !important",
    },
  },

  //Button
  buttonDetailToko: {
    margin: "20px 0 !important",
    width: "40% !important",
    padding: "8px 16px !important",
    borderRadius: "100px !important",

    fontWeight: "600 !important",
    lineHeight: "18px",
    textTransform: "capitalize !important",

    backgroundColor: `${theme.palette.custom.FirstGreen} !important`,
    boxShadow: "0px 8px 16px rgba(3, 172, 14, 0.24) !important",

    "@media screen and (max-width: 560px)": {
      width: "90% !important",
    },
  },
});

export default styles;
