import React from "react";
import { Button } from "@mui/material";

function ButtonDetailToko(props) {
  const { style } = props;
  return (
    <Button className={style.buttonDetailToko} variant="contained">
      Jadikan Toko Favorit
    </Button>
  );
}

export default ButtonDetailToko;
