import React from "react";
import Header from "./header/index";
import List from "./list-product/index";

import { Container } from "@mui/material";

function Component(props) {
  const { classes } = props;
  return (
    <Container className={classes.containerApp}>
      <Header style={classes} />
      <List style={classes} />
    </Container>
  );
}

export default Component;
