import React, { useContext } from "react";
import { ProductContext } from "../../../Context/ListProductContext";

import { Container, Box, Typography, Button } from "@mui/material";
import NumberFormat from "react-number-format";

function ListProductOther(props) {
  const { style } = props;
  const { list } = useContext(ProductContext);

  return (
    <Container className={style.containerProduct}>
      {list?.products?.map((data) => (
        <Box className={style.listBox} key={data.id}>
          <img
            className={style.thumbnailProduct}
            src={data.thumbnail}
            alt="thumbnail"
          />
          <Typography className={style.titleProduct}>{data.title}</Typography>
          <Typography className={style.priceProduct}>
            <NumberFormat
              value={data.price}
              displayType={"text"}
              prefix={"$"}
            />
          </Typography>
          <Button variant="outlined" className={style.buttonAdd}>
            Tambah
          </Button>
        </Box>
      ))}
    </Container>
  );
}

export default ListProductOther;
