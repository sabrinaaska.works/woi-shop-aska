const styles = (theme) => ({
  //App
  filterContainer: {
    padding: "20px 0 !important",
    display: "flex !important",
    flexDirection: "row !important",
    justifyContent: "space-between !important",
    alignItems: "center !important",
  },

  search: {
    width: "85% !important",
    height: "50px !important",
    borderRadius: "100px !important",
    "@media screen and (max-width: 380px)": {
      width: "80% !important",
    },
    "@media screen and (max-width: 285px)": {
      width: "75% !important",
    },
  },

  iconButton: {
    width: "45px !important",
    height: "45px !important",
    backgroundColor: `${theme.palette.custom.FirstGreen} !important`,
  },

  iconFilter: {
    fill: "#FFFFFF !important",
  },
});

export default styles;
