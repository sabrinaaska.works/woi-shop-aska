import React from "react";

import {
  Container,
  OutlinedInput,
  InputAdornment,
  IconButton,
} from "@mui/material";
import SearchIcon from "@mui/icons-material/Search";
import FilterAltIcon from "@mui/icons-material/FilterAlt";

function Search(props) {
  const { style } = props;

  return (
    <Container className={style.filterContainer}>
      <OutlinedInput
        required
        color="success"
        placeholder="Cari Produk"
        startAdornment={
          <InputAdornment position="start">
            <SearchIcon />
          </InputAdornment>
        }
        className={style.search}
      />

      <IconButton className={style.iconButton}>
        <FilterAltIcon className={style.iconFilter} />
      </IconButton>
    </Container>
  );
}

export default Search;
