import React from "react";
import Search from "./search/index";

import { Container } from "@mui/material";

function Component(props) {
  const { classes } = props;

  return (
    <Container className={classes.containerApp}>
      <Search style={classes} />
    </Container>
  );
}

export default Component;
