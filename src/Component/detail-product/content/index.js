import React from "react";
import { Container, Typography, Button } from "@mui/material";
import NumberFormat from "react-number-format";

function Content(props) {
  const { style, data } = props;

  return (
    <Container>
      <div className={style.category}>{data.category}</div>
      <Typography className={style.title}>{data.title}</Typography>
      <Typography className={style.price}>
        <NumberFormat value={data.price} displayType={"text"} prefix={"$"} />
      </Typography>
      <hr className={style.hr} />
      <Typography className={style.descriptionTitle}>
        Deskripsi Produk
      </Typography>
      <Typography className={style.description}>{data.description}</Typography>
      <Button variant="contained" className={style.buttonAdd}>
        Tambah Pesanan
      </Button>
    </Container>
  );
}

export default Content;
