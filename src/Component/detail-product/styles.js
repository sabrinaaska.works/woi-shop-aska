const styles = (theme) => ({
  link: {
    outline: "none !important",
    textDecoration: "none  !important",
  },

  //App
  containerApp: {
    position: "relative !important",
    padding: "4rem 0 0 0 !important",
    display: "flex !important",
    flexDirection: "column !important",
    alignItems: "center !important",
  },

  //Header
  containerHeader: {
    top: 0,
    position: "fixed !important",
    maxWidth: "500px !important",
    padding: "10px  16px !important",
    zIndex: "100 !important",

    display: "flex !important",
    alignItems: "flex-start !important",
    justifyContent: "space-between !important",

    gap: "10px",
    boxShadow: "0px 2px 8px rgba(0, 0, 0, 0.04)",
    "@media screen and (max-width: 560px)": {
      fontSize: "14px !important",
    },
    backgroundColor: theme.palette.custom.White,

    "@media screen and (max-width: 285px)": {
      maxWidth: "100% !important",
      width: "100% !important",
    },
  },

  boxHeader: {
    display: "flex !important",
    alignItems: "center !important",
    justifyContent: "center !important",
  },

  arrowBack: {
    color: "#03AC0E !important",
  },

  textNav: {
    marginLeft: "10px !important",
    fontWeight: "600 !important",
    lineHeight: "24px !important",
    color: theme.palette.custom.BlackText,

    "@media screen and (max-width: 560px)": {
      fontSize: "14px !important",
      lineHeight: "22px !important",
    },
  },

  //Carousel
  carouselBox: {
    padding: "0 16px !important",
    display: "flex !important",
    flexGrow: "1 !important",
    justifyContent: "center !important",
    alignItems: "center !important",
  },

  imgCarouselBox: {
    width: "100% !important",
    height: "250px !important",
    overflow: "hidden !important",
    objectFit: "cover !important",
    display: "flex !important",
    justifyContent: "center !important",
    alignItems: "center !important",
  },

  //Content
  category: {
    marginTop: "20px !important",
    width: "fit-content !important",
    padding: "8px !important",
    borderRadius: "100px !important",
    backgroundColor: "#EBEBEB !important",
    fontWeight: "600 !important",
    color: theme.palette.custom.Grey,

    "@media screen and (max-width: 560px)": {
      fontSize: "14px !important",
    },
  },

  title: {
    marginTop: "16px !important",
    fontWeight: "600 !important",
    color: theme.palette.custom.BlackText,

    "@media screen and (max-width: 560px)": {
      fontSize: "14px !important",
    },
  },

  price: {
    marginBottom: "20px !important",
    fontWeight: "600 !important",
    color: theme.palette.custom.BlackText,
    "@media screen and (max-width: 560px)": {
      fontSize: "14px !important",
    },
  },

  hr: {
    marginBottom: "20px !important",
    height: "1px !important",
    backgroundColor: "#EBEBEB !important",
    border: "none",
  },

  descriptionTitle: {
    marginBottom: "5px !important",
    fontWeight: "600 !important",
    "@media screen and (max-width: 560px)": {
      fontSize: "14px !important",
    },
  },

  description: {
    fontWeight: "400 !important",
    color: theme.palette.custom.Grey,
    "@media screen and (max-width: 560px)": {
      fontSize: "14px !important",
    },
  },

  buttonAdd: {
    margin: "20px 0 !important",
    padding: "10px 0 !important",
    width: "100% !important",

    backgroundColor: `${theme.palette.custom.FirstGreen} !important`,
    borderRadius: "100px !important",
    textTransform: "capitalize !important",
    fontWeight: "600 !important",
    "@media screen and (max-width: 560px)": {
      fontSize: "14px !important",
    },
  },
});

export default styles;
