import React from "react";
import { useNavigate } from "react-router-dom";

import { Container, Box, Typography, IconButton } from "@mui/material";
import ArrowBackIcon from "@mui/icons-material/ArrowBack";

function HeaderDetailToko(props) {
  const { style, data } = props;
  const navigate = useNavigate();
  return (
    <Container className={style.containerHeader}>
      <Box className={style.boxHeader}>
        <IconButton onClick={() => navigate(-1)}>
          <ArrowBackIcon className={style.arrowBack} />
        </IconButton>
        <Typography className={style.textNav}>{data.title}</Typography>
      </Box>
    </Container>
  );
}

export default HeaderDetailToko;
