import React, { useState, useEffect } from "react";
import { useParams } from "react-router-dom";
import axios from "axios";
import Header from "./header/index";
import Content from "./content/index";
import Carousel from "./carousel/index";

import { Container } from "@mui/material";

function Component(props) {
  const { classes } = props;

  //get details product
  const { id } = useParams();
  const [details, setDetails] = useState({});

  useEffect(() => {
    const fetch = async () => {
      try {
        const { data } = await axios.get(
          `https://dummyjson.com/products/${id}`
        );
        setDetails(data);
      } catch (error) {
        console.log(error);
      }
    };
    fetch();
  }, [id]);

  return (
    <Container className={classes.containerApp}>
      <Header data={details} style={classes} />
      <Carousel style={classes} data={details} />
      <Content style={classes} data={details} />
    </Container>
  );
}

export default Component;
