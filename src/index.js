import React from "react";
import ReactDOM from "react-dom/client";
import "./index.css";
import App from "./App";
import reportWebVitals from "./reportWebVitals";

import { createTheme, ThemeProvider } from "@mui/material/styles";
import { BrowserRouter } from "react-router-dom";

import ListProductProvider from "./Context/ListProductContext";
import ListTokoProvider from "./Context/ListProductToko";

const theme = createTheme({
  typography: {
    fontFamily: `"Rubik", sans-serif`,
  },
  palette: {
    custom: {
      White: "#FFFFFF",
      BlackText: "#292929",
      Grey: "#7A7A7A",
      Rectangle: "#F5F5F5",
      FirstGreen: "#03AC0E",
      SecondGreen: "#049C0E",
      ThirdGreen:
        "linear-gradient(88.88deg, #03AC0E 0%, rgba(23, 194, 34, 0.69) 99.99%, rgba(3, 172, 14, 0.44) 100%)",
      FourthGreen: "linear-gradient(57.21deg, #00E40F 0%, #017109 100%)",
      FifthGreen: "rgba(3, 172, 14, 0.22)",
    },
  },
});

const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(
  <BrowserRouter>
    <ListTokoProvider>
      <ListProductProvider>
        <ThemeProvider theme={theme}>
          <App />
        </ThemeProvider>
      </ListProductProvider>
    </ListTokoProvider>
  </BrowserRouter>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
