const styles = () => ({
  containerApp: {
    padding: "0 !important",
    maxWidth: "500px",
    backgroundColor: "#ffffff",
    minHeight: "120vh",
    width: "100%",
    "@media (max-width: 560px)": {
      maxWidth: "100vw",
    },
  },
});

export default styles;
