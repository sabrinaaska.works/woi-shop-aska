import React, { useState, useEffect, createContext } from "react";
import axios from "axios";

export const ProductContext = createContext();

function ListProductProvider(props) {
  const [list, setList] = useState([]);

  useEffect(() => {
    const getList = async () => {
      try {
        const resList = await axios({
          method: "GET",
          url: "https://dummyjson.com/products",
        });
        setList(resList.data);
      } catch (err) {
        console.log(err);
      }
    };
    getList();
  }, []);

  //filter products

  const filterListSmartphones = list?.products?.filter((data) => {
    return data.category === "smartphones";
  });

  const filterListLaptops = list?.products?.filter((data) => {
    return data.category === "laptops";
  });

  const filterListFragrances = list?.products?.filter((data) => {
    return data.category === "fragrances";
  });

  const filterListSkincare = list?.products?.filter((data) => {
    return data.category === "skincare";
  });

  const filterListGroceries = list?.products?.filter((data) => {
    return data.category === "groceries";
  });

  const filterListHomeDecoration = list?.products?.filter((data) => {
    return data.category === "home-decoration";
  });

  const filterListFreezeFood = list?.products?.filter((data) => {
    return data.category === "freeze-food";
  });

  return (
    <ProductContext.Provider
      value={{
        list,
        filterListSmartphones,
        filterListLaptops,
        filterListFragrances,
        filterListSkincare,
        filterListGroceries,
        filterListHomeDecoration,
        filterListFreezeFood,
      }}
    >
      {props.children}
    </ProductContext.Provider>
  );
}

export default ListProductProvider;
