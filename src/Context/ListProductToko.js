import React, { useState, useEffect, createContext } from "react";
import axios from "axios";

export const TokoContext = createContext();

function ListTokoProvider(props) {
  const [category, setCategory] = useState([]);

  useEffect(() => {
    const getList = async () => {
      try {
        const resList = await axios({
          method: "GET",
          url: "https://dummyjson.com/products/categories",
        });
        setCategory(resList.data);
      } catch (err) {
        console.log(err);
      }
    };
    getList();
  }, []);

  const Icon = ({ category }) => {
    if (category === "smartphones") return "./img-category/smartphones.png";
    if (category === "laptops") return "./img-category/laptops.png";
    if (category === "fragrances") return "./img-category/fragrances.png";
    if (category === "skincare") return "./img-category/skincare.png";
    if (category === "groceries") return "./img-category/groceries.png";
    if (category === "home-decoration")
      return "./img-category/home-decoration.png";
    if (category === "furniture") return "./img-category/furniture.png";
    if (category === "tops") return "./img-category/tops.png";
    if (category === "womens-dresses")
      return "./img-category/womens-dresses.png";
    if (category === "womens-shoes") return "./img-category/womens-shoes.png";
    if (category === "mens-shirts") return "./img-category/mens-shirts.png";
    if (category === "mens-shoes") return "./img-category/mens-shoes.png";
    if (category === "mens-watches") return "./img-category/mens-watches.png";
    if (category === "womens-watches")
      return "./img-category/womens-watches.png";
    if (category === "womens-bags") return "./img-category/womens-bags.png";
    if (category === "womens-jewellery")
      return "./img-category/womens-jewellery.png";
    if (category === "sunglasses") return "./img-category/sunglasses.png";
    if (category === "automotive") return "./img-category/automotive.png";
    if (category === "motorcycle") return "./img-category/motorcycle.png";
    if (category === "lighting") return "./img-category/lighting.png";

    return "./list-category/photo_2022-08-16_22-17-53.jpg";
  };

  return (
    <TokoContext.Provider
      value={{
        category,
        Icon,
      }}
    >
      {props.children}
    </TokoContext.Provider>
  );
}

export default ListTokoProvider;
